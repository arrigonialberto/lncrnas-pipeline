#!/usr/bin/env python
'''
   Use CPAT to classify the sequences contained in the input FASTA file
   Analyze CPAT output to update the 'noncoding status' of sequences
   in the sqlite3 database (boolean column, 1 means noncoding)
'''

import argparse
import os
import sqlite3

parser = argparse.ArgumentParser()
parser.add_argument("fasta_name", help="Input fasta file name")
parser.add_argument("species", help="Human/Mouse")
parser.add_argument("out_file", help="Name of the output file")
parser.add_argument("dat_folder", help="Path to model dat file for CPAT")
args = parser.parse_args()
fasta_file = args.fasta_name
species_name = args.species
out_file = args.out_file

DAT_FOLDER = args.dat_folder
CPAT = '$(which cpat.py)'
species = {'human':'-d {dat_folder}/Human_logitModel.RData -x {dat_folder}/Human_Hexamer.tsv','mouse':'-d {dat_folder}/Mouse_logitModel.RData -x {dat_folder}/Mouse_Hexamer.tsv'}
FASTA_FILE = '-g '+fasta_file
OUT_FILE = '-o '+out_file

command_line = "{cpat} {species} {fasta} {out_file}".format(cpat=CPAT,species=species[species_name].format(dat_folder=DAT_FOLDER),fasta=FASTA_FILE,out_file=OUT_FILE)
print command_line
os.system(command_line)

def update_noncoding_status(fasta_file,record_name):
        db_connection = sqlite3.connect('{0}.db'.format(fasta_file))
        db_cursor = db_connection.cursor()
	print 'Noncoding CPAT: {name}'.format(name=record_name)
        db_cursor.execute("UPDATE reads SET noncoding_cpat={col_update} WHERE name_fasta='{name}'".format(col_update=1,name=record_name))
        db_connection.commit()
        db_connection.close()

def send_cpat_to_db(cpat_output,cpat_threshold):
    for line in open(cpat_output,'r'):
        if line.split()[0]=='mRNA_size': continue
        fasta_id = line.split()[0]
        score = line.split()[5]
        if float(score)<cpat_threshold:
            update_noncoding_status(fasta_file,fasta_id)

thresholds={'human':0.364, 'mouse':0.44}
send_cpat_to_db(out_file,thresholds[species_name])

    
