#!/usr/bin/env python
from optparse import OptionParser
from BCBio import GFF
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.SeqRecord import SeqRecord
import subprocess
import os
import sys
import re

def lookupSequences(files):
    gtf_file = open(files['gtf_file'])
    records = []
    limit_info = dict(gff_type = ["exon"])
    fasta = SeqIO.to_dict(SeqIO.parse(open(files['seq_file']),'fasta'))
    for rec in GFF.parse(gtf_file,limit_info=limit_info):
        if rec.id[:3]!='chr': 
            chrom='chr'+rec.id
        else:
            chrom=rec.id
        for feature in rec.features:
            if chrom=='chrMT':continue
            id=''
            seq=''
            if feature.sub_features == []:
                try:
                    seq = fasta[chrom].seq[(feature.location.start):feature.location.end]
                except KeyError:
                    raise Exception('Chromosome feature {0} does not match fasta identifier {1}'.format(chrom,rec.id))
                id = feature.qualifiers['Name'][0]
                strand = feature.strand
            else:
                id = feature.id
                for subf in feature.sub_features:
	            try:
                        seq = seq + fasta[chrom].seq[(subf.location.start):subf.location.end]
                    except KeyError:
                        raise Exception('Chromosome feature {0} does not match fasta identifier {1}'.format(chrom,rec.id))
                strand = subf.strand
            if strand is -1:
               seq = seq.reverse_complement()
            print ">"+re.sub(r'.mrna1$','',id)
            print seq

def main():
    usage = "usage: gtf2fasta seq_file gtf_file"
    parser = OptionParser()
    (options, args) = parser.parse_args()

    files = {}

    if len(args) != 2:
        print usage
        exit(-1)

    files['seq_file'] = args[0]
    files['gtf_file'] = args[1]

    if not os.path.exists(files['seq_file']):
        print "seq_file does not exist"
        print usage
        exit(-1)

    if not os.path.exists(files['gtf_file']):
        print "gtf_file does not exist"
        print usage
        exit(-1)
    lookupSequences(files)
    
if __name__ == "__main__":
    main()
