#!/usr/bin/env python
'''
   Use PLEK to classify the sequences contained in the input FASTA file
   Analyze PLEK output to update the 'noncoding status' of sequences
   in the sqlite3 database (boolean column, 1 means noncoding)
'''

import argparse
import os
import sqlite3

#Command line: python PLEK.py -fasta PLEK_test.fa -out predicted -thread 10

parser = argparse.ArgumentParser()
parser.add_argument("fasta_name", help="Input fasta file name")
parser.add_argument("out_file", help="Name of the output file")
parser.add_argument("n_thread", help="Number of threads")
args = parser.parse_args()
fasta_file = args.fasta_name
out_file = args.out_file
n_thread = args.n_thread

PLEK = 'python $(which PLEK.py)'
FASTA_FILE = '-fasta '+fasta_file
OUT_FILE = '-out '+out_file

command_line = "{PLEK} {fasta} {out_file}".format(PLEK=PLEK,fasta=FASTA_FILE,out_file=OUT_FILE)
print command_line
os.system(command_line)

def update_noncoding_status(fasta_file,record_name):
        db_connection = sqlite3.connect('{0}.db'.format(fasta_file))
        db_cursor = db_connection.cursor()
	print 'Updating PLEK noncoding status of {name}'.format(name=record_name)
        db_cursor.execute("UPDATE reads SET noncoding_plek={col_update} WHERE name_fasta='{name}'".format(col_update=1,name=record_name))
        db_connection.commit()
        db_connection.close()

def send_plek_to_db(cpat_output):
    for line in open(cpat_output,'r'):
        fasta_id = line.split()[2]
        score = line.split()[0]
        if score=='Non-coding':
            update_noncoding_status(fasta_file,fasta_id[1:].upper())

send_plek_to_db(out_file)

    
