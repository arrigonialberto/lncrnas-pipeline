#!/usr/bin/env python
import re
import miscBio
import argparse
import sqlite3

parser = argparse.ArgumentParser()
parser.add_argument("fasta_name", help="Input fasta file name")
parser.add_argument("blasr_output", help="Blasr output file, options -m 4 -header")
args = parser.parse_args()
blasr_output = args.blasr_output
fasta_file = args.fasta_name

db_connection = sqlite3.connect('{0}.db'.format(fasta_file))
db_cursor = db_connection.cursor()

reader = miscBio.BLASRm4Reader(blasr_output)
query_list = []
match_flag=dict()

for m in reader:
    qID = m.qID
    # fix qID extracted from Blasr output to match fasta_id from fasta input file
    if re.match(r".*0_(\d+)$", qID): qID=re.sub(r"/0_\d+$",'', qID)
    
    if qID in match_flag: continue
    len_q_match = float(str(m).split("\n")[8].strip().split()[1].split("-")[1])-float(str(m).split("\n")[8].strip().split()[1].split('-')[0])
    #len_s_match = float(str(m).split("\n")[9].strip().split()[1].split("-")[1])-float(str(m).split("\n")[9].strip().split()[1].split('-')[0])
    if float(m.identity)>90 and len_q_match/float(m.qLen)>0.90:
       # Update blasr_match column to 'annotated status' (boolean=0)
       db_cursor.execute("UPDATE reads SET blasr_match={col_update} WHERE name_fasta='{seq_id}'".format(col_update=1,seq_id=qID.upper()))
       db_connection.commit()
       match_flag[qID]=0
       print qID.upper()+" matching"
    else:
       pass
       
db_connection.close()

