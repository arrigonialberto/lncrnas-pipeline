#!/bin/bash

FASTA_FILE=$1
BLASR_PROC=$2
ORGANISM=$3

if [ "$3" = 'mouse' ]
then
  source data_config mouse
elif [ "$3" = 'human' ]
then
  source data_config human
else
  echo "ERROR: organism name missing"
  echo "Usage: cpat_pipeline.sh fasta_file n_proc organism"
fi

echo 'Mapping reads to reference genome...'
#Align FASTA using GMAP (output is .gtf)
gmap -D $GMAP_DB  -d $GMAP_REF -f gff3_gene -n 0 -t $BLASR_PROC $FASTA_FILE > ${FASTA_FILE}.gtf 2> ${FASTA_FILE}.gtf.log

echo "Creating fasta file from gtf..."
gtf_to_fasta_mem.py $REF_GENOME ${FASTA_FILE}.gtf > ${FASTA_FILE%.*}_filtered.fa
FASTA_FILE=${FASTA_FILE%.*}_filtered.fa

echo "Creating seqs db..."
# Write sequences and metadata to sqlite db
send_fasta_to_db.py $FASTA_FILE

# Perform noncoding classification using 'cpat' on selected fasta
fasta_basename="${FASTA_FILE%.*}"
CPAT_FASTA_OUT="${fasta_basename}_cpat.out"

# Update 'noncoding' status on sqlite db for selected 
# sequences according to cpat results
cpat_analyze.py $FASTA_FILE $ORGANISM $CPAT_FASTA_OUT $CPAT_DATA_FOLDER

# Align reads using Blasr to Gencode protein coding transcripts 
blasr.sh $FASTA_FILE $BLASR_PROC $FASTA_TARGET_CODING
# Update 'noncoding' status on sqlite db for selected sequences
# according to Gencode alignment results
analyze_blasr_output.py $FASTA_FILE "gencode_${FASTA_FILE}.txt"

# Align reads using Blasr to Gencode lncRNA transcripts 
blasr.sh $FASTA_FILE $BLASR_PROC $FASTA_TARGET_NCRNA
# Update 'noncoding' status on sqlite db for selected sequences
# according to Gencode alignment results
analyze_blasr_output.py $FASTA_FILE "gencode_${FASTA_FILE}.txt"

# Extract fasta IDs from db of sequences
# classified as 'noncoding' by CPAT that 
# also pass the hmmer filtering step
extract_noncoding_fasta.py $FASTA_FILE --cpat

BASE="${FASTA_FILE%.*}"
grep ">" ${BASE}_ncrna.fa > ncrna_${FASTA_FILE}_list

if [ -e "${BASE}_ncrna.fa" ] && [ -e "ncrna_${FASTA_FILE}_list" ]
then
    echo "Output files have been created:"
    echo "ncrna_${FASTA_FILE}_list"
    echo "${FASTA_FILE}_ncrna.fa"
fi

