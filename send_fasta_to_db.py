#!/usr/bin/env python
import sqlite3
from Bio import SeqIO
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("fasta_name", help="Input fasta file name")
args = parser.parse_args()
fasta_file = args.fasta_name

if os.path.isfile("{0}.db".format(fasta_file)):
    os.system('rm {0}.db'.format(fasta_file))

conn = sqlite3.connect('{0}.db'.format(fasta_file))
c = conn.cursor()	
c.execute('''DROP TABLE IF EXISTS reads''')
conn.commit()
c.execute('''CREATE TABLE reads
	             (idx integer,name text,name_fasta text, sequence text, noncoding_cpat integer, noncoding_plek integer, blasr_match integer)''')
conn.commit()	

print 'Writing fasta to db...'

count = 0
for record in SeqIO.parse(fasta_file,'fasta'):
    if len(record.seq)>200:
        count+=1
        c.execute("insert into reads values (?,?,?,?,?,?,?)", (count,record.description,record.name.upper(), str(record.seq), 0, 0, 0))
        conn.commit()

c.execute('''CREATE UNIQUE INDEX idx_seq
on reads(idx);''')
conn.commit()
c.execute('''CREATE UNIQUE INDEX idx_name
on reads(name);''')
conn.commit()
c.execute('''CREATE UNIQUE INDEX idx_name_fasta
on reads(name_fasta);''')
conn.commit()

conn.close()
