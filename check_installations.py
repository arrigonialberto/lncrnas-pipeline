#!/usr/bin/env python

import os
import sys

def which(program):

    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

def check_plek():
    plek = which('PLEK.py')
    if plek is None:
        print "ERROR: 'plek.py' must executable, please add it to your path or install it"
        exit(1)

def check_cpat():
    cpat = which('cpat.py')
    if cpat is None:
        print "ERROR: 'cpat.py' must executable, please add it to your path or install it"
        exit(1)

def check_blasr():
    blasr = which('blasr')
    if blasr is None:
        print "ERROR: 'blasr' must be installed to execute cpat_pipeline"
        exit(1)

def check_gmap():
    gmap = which('gmap')
    if gmap is None:
        print "ERROR: 'gmap' must be installed to execute cpat_pipeline"
        exit(1)

def main():
    if sys.argv[1] == 'plek': 
        check_plek()
    else:
        check_cpat()
        check_blasr()
        check_gmap()


if __name__ == "__main__":
    main()







