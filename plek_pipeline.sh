#!/bin/bash

BLASR_PROC=$2

FASTA_FILE=$1

echo "Creating seqs db..."
# Write sequences and metadata to sqlite db
send_fasta_to_db.py $FASTA_FILE

# Perform noncoding classification using 'cpat' on selected fasta
fasta_basename="${FASTA_FILE%.*}"
PLEK_FASTA_OUT="${fasta_basename}_plek.out"

# Update 'noncoding' status on sqlite db for selected 
# sequences according to cpat results
plek_analyze.py $FASTA_FILE $PLEK_FASTA_OUT $BLASR_PROC

# Extract fasta IDs from db of sequences
# classified as 'noncoding' by CPAT that 
# also pass the hmmer filtering step
extract_noncoding_fasta.py $FASTA_FILE --plek
BASE="${FASTA_FILE%.*}"
grep ">" ${BASE}_ncrna.fa > ncrna_${FASTA_FILE}_list

if [ -e "${BASE}_ncrna.fa" ] && [ -e "ncrna_${FASTA_FILE}_list" ]
then
    echo "Output files have been created:"
    echo "ncrna_${FASTA_FILE}_list"
    echo "${FASTA_FILE}_ncrna.fa"
fi

