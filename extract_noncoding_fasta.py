#!/usr/bin/env python

'''Generate a fasta file containing the sequences classified as 
   'noncoding' by CPAT and also passing the hmmer filtering step '''

import sqlite3
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("fasta_name", help="Input fasta file name")
parser.add_argument("--cpat", help="Restrict search only to nc-classified transcripts from CPAT", action='store_true')
parser.add_argument("--plek", help="Restrict search only to nc-classified transcripts from BLASR", action='store_true')
args = parser.parse_args()
fasta_file = args.fasta_name

db_connection = sqlite3.connect('{0}.db'.format(fasta_file))
db_cursor = db_connection.cursor()
if args.cpat: rows = db_cursor.execute('select * from reads where blasr_match=0 and noncoding_cpat=1')
if args.plek: rows = db_cursor.execute('select * from reads where noncoding_plek=1')

def splitCount(s, count):
    """ 
    Used to print formatted fasta sequences
    """
    return [s[i:i+count] for i in range(0, len(s), count)]

with open('{0}_ncrna.fa'.format(os.path.splitext(fasta_file)[0]),'w') as f:
    for row in rows:
    # row[1] is the fasta header, row[3] is the fasta sequence
        f.write(">"+row[1]+"\n")   
        for i in splitCount(row[3],80): f.write(i+"\n")

db_connection.close()
