import os
from setuptools import setup, find_packages
from distutils.extension import Extension

setup(
        name = 'Lncrna_pipeline',
        version = '1.0', 
        author = 'Alberto Arrigoni',
        author_email = 'arrigonialberto86@gmail.com',
        install_requires = [
            'numpy',
            'biopython >= 1.6',
            'bcbio-gff',
            ]
)
