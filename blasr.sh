#!/bin/bash

if [ "$1" = "" ]
then
  echo "Usage: blasr.sh <fasta_name> <n_processes> <fasta_target>"
  exit
fi

BLASR_EXEC=blasr
FASTA_FILE=$1
PROC=$2
FASTA_TARGET=$3
OUTPUT_FILE="gencode_${FASTA_FILE}.txt"
$BLASR_EXEC $FASTA_FILE $FASTA_TARGET -nproc $PROC -m 4 -header > $OUTPUT_FILE



